try {
  var output = {status: 0, result: []},
      params = JSON.parse(value),
      req = new HttpRequest(),
      resp

  req.addHeader('Content-Type: application/json')

  resp = req.get(params.url + '/api/info')
  var cid = JSON.parse(resp).result.omadacId
  var base_url = params.url + '/' + cid

  resp = req.post(base_url + '/api/v2/login',
                  JSON.stringify({username: params.username, password: params.password}))
  var token = JSON.parse(resp).result.token

  req.addHeader('Csrf-Token: ' + token)

  resp = req.get(base_url + '/api/v2/sites/' + params.siteId + '/eaps/' + params.mac)
  var device = JSON.parse(resp).result

  output.result = device

  req.post(base_url + '/api/v2/logout')

} catch (error) {
  Zabbix.log(2, '[Omada]: ' + error)
  output.status = 1
}

return JSON.stringify(output)
