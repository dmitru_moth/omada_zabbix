try {
  var output = {status: 0, result: []},
      params = JSON.parse(value),
      req = new HttpRequest(),
      ip = params.ip.split('.').slice(0, 3).join('.'),
      resp

  req.addHeader('Content-Type: application/json')

  resp = req.get(params.url + '/api/info')
  var cid = JSON.parse(resp).result.omadacId
  var base_url = params.url + '/' + cid

  resp = req.post(base_url + '/api/v2/login',
                  JSON.stringify({username: params.username, password: params.password}))
  var token = JSON.parse(resp).result.token

  req.addHeader('Csrf-Token: ' + token)

  resp = req.get(base_url + '/api/v2/users/current')
  var sites = JSON.parse(resp).result.privilege.sites

  for (var i in sites) {
    var site = sites[i]

    resp = req.get(base_url + '/api/v2/sites/' + site.key + '/devices')
    var devices = JSON.parse(resp).result

    for (var j in devices) {
      var device = devices[j]
      if (device.ip === undefined)
        continue
      if (!device.ip.startsWith(ip))
        continue
      output.result.push(device)
    }
  }

  req.post(base_url + '/api/v2/logout')

} catch (error) {
  Zabbix.log(2, '[Omada]: ' + error)
  output.status = 1
}

return JSON.stringify(output)
